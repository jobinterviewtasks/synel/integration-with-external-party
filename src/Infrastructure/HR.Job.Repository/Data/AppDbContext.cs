﻿using Microsoft.EntityFrameworkCore;
using HR.Job.Domain.EntityClasses;

namespace HR.Job.Repository;
public class AppDbContext : DbContext
{
    public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }

    public DbSet<Employee> Employees { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Employee>()
            .HasKey(m => m.Id);

        modelBuilder.Entity<Employee>()
            .Property(m => m.DateOfBirth)
            .HasColumnType("date");

        modelBuilder.Entity<Employee>()
            .Property(m => m.StartDate)
            .HasColumnType("date");
    }
}
