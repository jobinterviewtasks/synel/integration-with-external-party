﻿using HR.Job.Application;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.DependencyInjection;

namespace HR.Job.Repository;

public class UnitOfWork : IUnitOfWork
{
    private readonly IServiceProvider _serviceProvider;

    public UnitOfWork(IServiceProvider serviceProvider)
    {
        _serviceProvider = serviceProvider;
        this.Context = _serviceProvider.GetRequiredService<AppDbContext>();
    }

    public AppDbContext Context { get; }

    public IDbContextTransaction CurrencyTransaction { get => Context.Database.CurrentTransaction!; }

    public IEmployeeRepository EmployeeRepository { get { return GetRepository<IEmployeeRepository>(); } }

    public IDbContextTransaction BeginTransaction()
    {
        return Context.Database.BeginTransaction();
    }

    public Repository GetRepository<Repository>()
    {
        return _serviceProvider.GetRequiredService<Repository>();
    }

    public void Save() => Context.SaveChanges();

    public void Commit() { Save(); if (Context.Database.CurrentTransaction is not null) Context.Database.CurrentTransaction.Commit(); }

    public void Rollback() { if (Context.Database.CurrentTransaction is not null) { Context.Database.CurrentTransaction.Rollback(); } }
}
