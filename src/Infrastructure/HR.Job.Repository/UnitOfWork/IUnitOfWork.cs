﻿using HR.Job.Application;
using Microsoft.EntityFrameworkCore.Storage;

namespace HR.Job.Repository;

public interface IUnitOfWork
{
    AppDbContext Context { get; }

    IDbContextTransaction CurrencyTransaction { get; }

    Repository GetRepository<Repository>();

    IDbContextTransaction BeginTransaction();

    IEmployeeRepository EmployeeRepository { get; }

    void Save();

    void Commit();

    void Rollback();
}
