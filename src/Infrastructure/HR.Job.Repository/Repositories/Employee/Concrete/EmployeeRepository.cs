﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using HR.Job.Application;
using HR.Job.Application.Services;
using HR.Job.Domain.EntityClasses;

namespace HR.Job.Repository;

/// <summary>
/// class that works with the database
/// </summary>
public class EmployeeRepository : IEmployeeRepository
{
    private readonly IMapper _mapper;
    private readonly IUnitOfWork _unitOfOfWork;
    public EmployeeRepository(IMapper mapper, IUnitOfWork unitOfOfWork)
    {
        _mapper = mapper;
        _unitOfOfWork = unitOfOfWork;
    }

    public long Create(Employee dto)
    {
        _unitOfOfWork.Context.Employees.Add(dto);
        _unitOfOfWork.Save();

        return dto.Id;
    }

    public void CreateRange(List<Employee> dto)
    {
        _unitOfOfWork.Context.Employees.AddRange(dto);
        _unitOfOfWork.Save();
    }

    public void Delete(long id)
    {
        var emp = _unitOfOfWork.Context.Employees.FirstOrDefault(x => x.Id == id);
        _unitOfOfWork.Context.Employees.Remove(emp);
        _unitOfOfWork.Context.Employees.Entry(emp).State = Microsoft.EntityFrameworkCore.EntityState.Deleted;
        _unitOfOfWork.Save();
    }

    public bool Exist(long id)
    {
        var emp = _unitOfOfWork.Context.Employees.FirstOrDefault(x => x.Id == id);

        return emp is not null;
    }

    public Employee GetById(long? id)
    {
        var emp = _unitOfOfWork.Context.Employees.FirstOrDefault(x => x.Id == id);
        return emp!;
    }

    public IQueryable<EmployeeGetListResDto> GetList(EmployeeGetListReqDto dto)
    {
        var query = _unitOfOfWork.Context.Employees.ProjectTo<EmployeeGetListResDto>(_mapper.ConfigurationProvider);
        return query;
    }

    public void Update(Employee dto)
    {
        var emp = _unitOfOfWork.Context.Employees.FirstOrDefault(x => x.Id == dto.Id);
        _unitOfOfWork.Context.Employees.Entry(emp).State = Microsoft.EntityFrameworkCore.EntityState.Detached;
        _unitOfOfWork.Context.Employees.Entry(dto).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
        _unitOfOfWork.Save();
    }
}
