using AutoMapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace HR.Job.Application;
public static class ApplicationExtensions
{
    public static void ConfigurApplicationServices(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddScoped<IEmployeeService, EmployeeService>();
        services.AddSingleton<EmployeeProfile>();
        services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
    }
}
