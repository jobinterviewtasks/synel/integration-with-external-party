﻿using CsvHelper;
using CsvHelper.Configuration;
using CsvHelper.TypeConversion;
using Microsoft.AspNetCore.Http;
using System.Globalization;

namespace HR.Job.Application.Utils;

public static class Util
{
    /// <summary>
    /// method that maps the received CSV file
    /// </summary>
    /// <param name="formFile"></param>
    /// <returns></returns>
    public static List<T> ParseCSVToDto<T, TRegClassMap>(IFormFile formFile)
        where T : class
        where TRegClassMap : ClassMap<T>
    {
        List<T> dtoList = new List<T>();

        using (var fileReader = new StreamReader(formFile.OpenReadStream()))
        {

            var config = new CsvConfiguration(CultureInfo.InvariantCulture)
            {
                HeaderValidated = null,
                MissingFieldFound = null,
                MemberTypes = MemberTypes.Fields
            };

            using var csv = new CsvReader(fileReader, config);
            var options = new TypeConverterOptions { Formats = new[] { "dd/M/yyyy" } };
            csv.Context.TypeConverterOptionsCache.AddOptions<DateTime?>(options);
            csv.Context.TypeConverterOptionsCache.AddOptions<DateTime>(options);
            csv.Context.RegisterClassMap<TRegClassMap>();
            csv.Read();
            csv.ReadHeader();
            dtoList = csv.GetRecords<T>().ToList();
        }
        return dtoList;
    }
}