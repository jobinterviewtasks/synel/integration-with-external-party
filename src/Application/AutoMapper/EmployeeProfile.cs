﻿using AutoMapper;
using HR.Job.Application.Services;
using HR.Job.Domain.EntityClasses;
using HR.Job.WebApp.Models;

namespace HR.Job.Application;
public class EmployeeProfile : Profile
{
    public EmployeeProfile()
    {
        CreateMap<EmployeeCreateReqDto, Employee>();
        CreateMap<EmployeeUpdateReqDto, Employee>();
        CreateMap<Employee, EmployeeGetByIdResDto>();
        CreateMap<Employee, EmployeeGetListResDto>();
        CreateMap<Employee, EmployeeAddFileResDto>();
        CreateMap<EmployeeCsvDto, EmployeeAddFileResDto>();
        CreateMap<EmployeeCsvDto, Employee>();
    }
}
