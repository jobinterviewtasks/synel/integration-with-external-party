﻿using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace HR.Job.Application.CustomValidations;

public class FileControllValidation : ValidationAttribute
{
    readonly string _fileFormat;
    public FileControllValidation(string fileFormat)
    {

        _fileFormat = fileFormat;
    }

    protected override ValidationResult IsValid(object value, ValidationContext validationContext)
    {
        IFormFile file = value as IFormFile;
        if (file != null)
        {
            if (file != null && !file.FileName.EndsWith(_fileFormat))
            {
                return new ValidationResult(ErrorMessageString);
            }
        }

        return ValidationResult.Success;
    }
}
