﻿using HR.Job.Application.CustomValidations;
using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace HR.Job.Application;

public class EmployeeAddFileReqDto
{
    [Display(Name = "Choose File")]
    [Required]
    [FileControllValidation("csv", ErrorMessage = "File format not correct. csv is required")]
    public IFormFile File { get; set; } = null!;
}
