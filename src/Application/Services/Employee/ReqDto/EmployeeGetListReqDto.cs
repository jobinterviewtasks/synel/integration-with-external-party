﻿namespace HR.Job.Application.Services;

public class EmployeeGetListReqDto : SortFilterPageOptions
{
    public string? Search { get; set; }
    public int countAdd { get; set; }
    //public Search Search { get; set; }

    public int OrderColumn { get; set; }
    public string OrderDir { get; set; }
}

public class Search
{
    public bool Regex { get; set; }
    public string Value { get; set; }
}
