﻿using CsvHelper.Configuration;
using HR.Job.Domain.EntityClasses;
using HR.Job.WebApp.Models;

namespace HR.Job.Application;

/// <summary>
/// Class created to map CSV file to Employee
/// </summary>
public sealed class EmployeeFileMap : ClassMap<EmployeeCsvDto>
{
    public EmployeeFileMap()
    {
        Map(m => m.PayrollNumber).Index(0);
        Map(m => m.Forename).Index(1);
        Map(m => m.Surname).Index(2);
        Map(m => m.DateOfBirth).Index(3);
        Map(m => m.Telephone).Index(4);
        Map(m => m.Mobile).Index(5);
        Map(m => m.Address).Index(6);
        Map(m => m.Address2).Index(7);
        Map(m => m.Postcode).Index(8);
        Map(m => m.EmailHome).Index(9);
        Map(m => m.StartDate).Index(10);
    }
}
