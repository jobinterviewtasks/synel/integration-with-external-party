﻿using HR.Job.Application.Services;

namespace HR.Job.Application;
public interface IEmployeeService
{
    PagedResult<EmployeeGetListResDto> GetList(EmployeeGetListReqDto dto);
    EmployeeGetByIdResDto GetById(long id);
    long Create(EmployeeCreateReqDto dto);
    void CreateRange(List<EmployeeCreateReqDto> dto);
    void Update(EmployeeUpdateReqDto dto);
    void Delete(long id);
    bool EmployeeExists(long id);
    List<EmployeeAddFileResDto> AddFile(EmployeeAddFileReqDto dto);
}
