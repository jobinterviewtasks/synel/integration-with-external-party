﻿using AutoMapper;
using HR.Job.Application.Services;
using HR.Job.Application.Utils;
using HR.Job.Domain.EntityClasses;
using HR.Job.WebApp.Models;
using System.Linq.Dynamic.Core;

namespace HR.Job.Application;

public class EmployeeService : IEmployeeService
{
    private readonly IMapper _mapper;
    private readonly IEmployeeRepository _employeeRepository;
    public EmployeeService(IMapper mapper, IEmployeeRepository employeeRepository)
    {
        _mapper = mapper;
        _employeeRepository = employeeRepository;
    }

    public long Create(EmployeeCreateReqDto dto)
    {
        var emp = _mapper.Map<Employee>(dto);
        dto.Id = _employeeRepository.Create(emp);

        return dto.Id;
    }

    public void CreateRange(List<EmployeeCreateReqDto> dto)
    {
        var empList = _mapper.Map<List<Employee>>(dto);
        _employeeRepository.CreateRange(empList);
    }

    public void Delete(long id)
    {
        _employeeRepository.Delete(id);
    }

    public bool EmployeeExists(long id)
    {
        return _employeeRepository.Exist(id);
    }

    public EmployeeGetByIdResDto GetById(long id)
    {
        var entity = _employeeRepository.GetById(id);
        var emp = _mapper.Map<EmployeeGetByIdResDto>(entity);
        return emp;
    }

    public PagedResult<EmployeeGetListResDto> GetList(EmployeeGetListReqDto dto)
    {
        var query = _employeeRepository.GetList(dto);

        dto.SortBy = dto.OrderColumn switch
        {
            0 => "id",
            1 => "surname",
            2 => "forename",
            3 => "payrollNumber",
            4 => "dateOfBirth",
            5 => "telephone",
            6 => "mobile",
            7 => "address",
            8 => "address2",
            9 => "postcode",
            10 => "emailHome",
            11 => "startDate",
        };

        query = query.OrderBy($"{dto.SortBy} {dto.OrderDir}");

        if (!string.IsNullOrWhiteSpace(dto.Search))
            query = query.Where(x =>
            x.Address.Contains(dto.Search) ||
            x.Address2.Contains(dto.Search) ||
            x.EmailHome.Contains(dto.Search) ||
            x.Forename.Contains(dto.Search) ||
            x.Surname.Contains(dto.Search) ||
            x.Mobile.Contains(dto.Search) ||
            x.PayrollNumber.Contains(dto.Search) ||
            x.Telephone.Contains(dto.Search) ||
            x.Postcode.Contains(dto.Search)
            );

        var res = query.AsPagedResult(dto);

        return res;
    }

    public void Update(EmployeeUpdateReqDto dto)
    {
        var emp = _mapper.Map<Employee>(dto);
        _employeeRepository.Update(emp);
    }

    public List<EmployeeAddFileResDto> AddFile(EmployeeAddFileReqDto dto)
    {
        var res = Util.ParseCSVToDto<EmployeeCsvDto, EmployeeFileMap>(dto.File);
        var empList = _mapper.Map<List<EmployeeAddFileResDto>>(res);
        var empListDb = _mapper.Map<List<Employee>>(res);

        _employeeRepository.CreateRange(empListDb);

        return empList;
    }
}
