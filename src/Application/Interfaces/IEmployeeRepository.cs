﻿using HR.Job.Application.Services;
using HR.Job.Domain.EntityClasses;

namespace HR.Job.Application;

public interface IEmployeeRepository
{
    IQueryable<EmployeeGetListResDto> GetList(EmployeeGetListReqDto dto);
    Employee GetById(long? id);
    long Create(Employee dto);
    void CreateRange(List<Employee> dto);
    void Update(Employee dto);
    void Delete(long id);
    bool Exist(long id);
}
