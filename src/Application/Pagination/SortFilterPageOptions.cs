using HR.Job.Application.Services;

namespace HR.Job.Application;
public class SortFilterPageOptions : PageOptions, ISortFilterOptions
{

    private const string ORDER_TYPE_ASC = "ASC";

    private const string ORDER_TYPE_DESC = "DESC";

    private string _orderType;
    public SortFilterPageOptions()
    {
        Init();
    }

    public SortFilterPageOptions(int defaultPageSize, int pageSizeLimit)
        : base(defaultPageSize, pageSizeLimit)
    {
        Init();
    }

   // public virtual string? Search { get; set; }

    public virtual string? SortBy { get; set; }

    public virtual string? OrderType
    {
        get => _orderType;
        set => _orderType = (new string[2] { ORDER_TYPE_ASC, ORDER_TYPE_DESC }.Contains(value.ToString().ToUpper()) ? value.ToUpper() : ORDER_TYPE_ASC);
    }

    public bool HasSearch() => /*!string.IsNullOrWhiteSpace(Search.Value)*/true;

    public bool HasSort() => !string.IsNullOrWhiteSpace(SortBy);

    private void Init() => _orderType = ORDER_TYPE_ASC;
}
