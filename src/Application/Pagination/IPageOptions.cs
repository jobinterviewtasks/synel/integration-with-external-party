﻿namespace HR.Job.Application;

public interface IPageOptions
{
    // PageNumber
    int Start { get; }
    // PageSize
    int Length { get; }
}
