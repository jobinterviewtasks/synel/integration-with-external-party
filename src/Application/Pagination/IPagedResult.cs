﻿namespace HR.Job.Application;

public interface IPagedResult
{
    int PageNumber { get; }

    int TotalPages { get; }

    int PageSize { get; }

    long TotalRowsCount { get; }

    bool HasPreviousPage { get; }

    bool HasNextpage { get; }
    string Error { get; set; }
}
