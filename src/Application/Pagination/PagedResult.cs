﻿
using Newtonsoft.Json;

namespace HR.Job.Application;
public class PagedResult<TRow> : IPagedResult
{
    [JsonProperty(PropertyName = "Draw")]
    public int PageNumber { get; set; } = 1;

    [JsonProperty(PropertyName = "RecordsFiltered")]
    public int PageSize { get; set; }

    [JsonProperty(PropertyName = "RecordsTotal")]
    public long TotalRowsCount { get; set; }

    [JsonProperty(PropertyName = "Data")]
    public IEnumerable<TRow> Rows { get; set; }

    public bool HasPreviousPage => PageNumber > 1;

    public bool HasNextpage => PageNumber < TotalPages;

    public int TotalPages
    {
        get
        {
            var _in_total = (int)Math.Ceiling(TotalRowsCount / (double)PageSize);
            if (_in_total < 0)
            {
                return 0;
            }

            return _in_total!;
        }
    }

    public string Error { get; set; }

    public PagedResult()
    {
    }

    public PagedResult(IPagedResult pagedResult, IEnumerable<TRow> rows)
        : this(pagedResult.PageNumber, pagedResult.PageSize, pagedResult.TotalRowsCount, rows)
    {
    }

    public PagedResult(int page, int pageSize, long total, IEnumerable<TRow> rows)
        : this()
    {
        PageNumber = page;
        PageSize = pageSize;
        TotalRowsCount = total;
        Rows = rows;
    }

    public long GetRowNumber(int rowIndex)
    {
        return rowIndex + (PageNumber - 1) * PageSize + 1;
    }

    public int GetIntRowNumber(int rowIndex)
    {
        return rowIndex + (PageNumber - 1) * PageSize + 1;
    }
}
