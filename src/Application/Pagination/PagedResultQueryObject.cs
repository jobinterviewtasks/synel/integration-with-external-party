namespace HR.Job.Application;
public static class PagedResultQueryObject
{
    public static PagedResult<TRow> AsPagedResult<TRow>(this IQueryable<TRow> query, IPageOptions options)
    {
       // return new PagedResult<TRow>(options.Start, options.Length, query.Count(), query.Skip(options.Length * (options.Start - 1)).Take(options.Length).AsEnumerable());
        return new PagedResult<TRow>(options.Start, options.Length, query.Count(), query.Skip(options.Start - 1).Take(options.Length).AsEnumerable());
    }
}
