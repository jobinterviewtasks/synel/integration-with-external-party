﻿namespace HR.Job.Application;

public class PageOptions : IPageOptions
{
    private int _page;

    private int _pageSize;

    private const int DEFAULT_PAGE_SIZE = 20;

    private const int DEFAULT_PAGE_SIZE_LIMIT = 1000;

    private readonly int _defaultPageSize = 20;

    private readonly int _pageSizeLimit = 1000;

    public virtual int Start { get => _page; set => _page = (value <= 0) ? 1 : value; }

    public int Length { get => _pageSize; set => _pageSize = (value > 0 && value <= _pageSizeLimit) ? value : _pageSizeLimit; }
    public PageOptions() : this(DEFAULT_PAGE_SIZE, DEFAULT_PAGE_SIZE_LIMIT) { }

    public PageOptions(int defPageSize, int defaultPageSize)
    {
        (_page, _pageSize, _defaultPageSize, _pageSizeLimit) = (1, defPageSize, defPageSize, defaultPageSize);
    }
}
