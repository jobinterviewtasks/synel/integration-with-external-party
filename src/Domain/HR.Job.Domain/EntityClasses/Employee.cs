namespace HR.Job.Domain.EntityClasses;

public class Employee
{
    public long Id { get; set; }

    public string PayrollNumber { get; set; } = null!;

    public string Forename { get; set; } = null!;

    public string Surname { get; set; } = null!;

    public DateTime DateOfBirth { get; set; }

    public string Telephone { get; set; } = null!;

    public string Mobile { get; set; } = null!;

    public string Address { get; set; } = null!;

    public string Address2 { get; set; } = null!;

    public string Postcode { get; set; } = null!;

    public string EmailHome { get; set; } = null!;

    public DateTime? StartDate { get; set; }
}
