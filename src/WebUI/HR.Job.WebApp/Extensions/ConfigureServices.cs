using Microsoft.EntityFrameworkCore;
using HR.Job.Repository;
using HR.Job.Application;

namespace HR.Job.WebApp;

public static class ConfigureServices
{
    public static IServiceCollection AddWebApplicationServices(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddPooledDbContextFactory<AppDbContext>(dbContextOptionsBuilder =>
        {
            dbContextOptionsBuilder.UseNpgsql(configuration.GetConnectionString("PostgreConnectionString"),
                builder => builder.MigrationsAssembly(typeof(AppDbContext).Assembly.FullName));
        });

        services.AddDbContext<AppDbContext>(options => options.UseNpgsql(configuration.GetConnectionString("PostgreConnectionString")));

        services.ConfigurRepositoryServices(configuration);

        services.ConfigurApplicationServices(configuration);

        return services;
    }
}
