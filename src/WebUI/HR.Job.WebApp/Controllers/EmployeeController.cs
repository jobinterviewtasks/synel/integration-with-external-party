﻿using HR.Job.Application;
using HR.Job.Application.Services;
using Microsoft.AspNetCore.Mvc;

namespace HR.Job.WebApp.Controllers
{
    public class EmployeeController : Controller
    {
        private readonly IEmployeeService _employeeService;

        public EmployeeController(IEmployeeService employeeService)
        {
            _employeeService = employeeService;
        }

        [HttpGet]
        public IActionResult Index(int countAdd = 0)
        {
            if (countAdd > 0)
            {
                ViewBag.Count = countAdd;
            }
            return View();
        }

        [HttpGet]
        public JsonResult GetList([FromQuery] EmployeeGetListReqDto dto)
        {
            var res = _employeeService.GetList(dto);

            if(dto.countAdd > 0)
            {
                ViewBag.Count = dto.countAdd;
            }

            return Json(new { data = res.Rows, recordsTotal = res.TotalRowsCount, recordsFiltered = res.TotalRowsCount });
        }

        [HttpGet]
        public IActionResult AddFile()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult AddFile(EmployeeAddFileReqDto dto)
        {
            if (ModelState.IsValid)
            {
                var res = _employeeService.AddFile(dto);
                return RedirectToAction(nameof(Index), new { countAdd = res.Count() });
            }
            return View(dto);
        }

        [HttpGet]
        public IActionResult Details(long id)
        {
            var dto = _employeeService.GetById(id);

            return View(dto);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(EmployeeCreateReqDto dto)
        {
            if (ModelState.IsValid)
            {
                var id = _employeeService.Create(dto);
                return RedirectToAction(nameof(Index));
            }

            return View(dto);
        }

        [HttpGet]
        public IActionResult Edit(long id)
        {
            var dto = _employeeService.GetById(id);

            return View(dto);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(EmployeeUpdateReqDto dto)
        {
            if (ModelState.IsValid)
            {
                _employeeService.Update(dto);
                return RedirectToAction(nameof(Index));
            }

            return View(dto);
        }

        [HttpGet]
        public IActionResult Delete(long id)
        {
            var dto = _employeeService.GetById(id);

            return View(dto);
        }

        [HttpPost, ActionName(nameof(Delete))]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(long id)
        {
            _employeeService.Delete(id);
            return RedirectToAction(nameof(Index));
        }
    }
}