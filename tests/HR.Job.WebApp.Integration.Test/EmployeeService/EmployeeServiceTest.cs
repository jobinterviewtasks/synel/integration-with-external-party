using NUnit.Framework;
using HR.Job.Application;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Http;
using FluentAssertions;
using HR.Job.Repository;
using Microsoft.EntityFrameworkCore;

namespace HR.Job.WebApp.Integration.Test.EmployeeService;

[TestFixture]
public class EmployeeServiceTest : BaseTestFixture
{
    long testId = 0;
    int csvCount = 0;
    string updatePostVal = "22";

    [TestCase(0)]
    [Test, Order(1)]
    public void AddFileTest(int param1)
    {
        int count = 0;
        using (var scope = Testing._scopeFactory.CreateScope())
        {
            var emp = scope.ServiceProvider.GetRequiredService<IEmployeeService>();

            EmployeeAddFileReqDto dto = null!;

            string currDir = Directory.GetCurrentDirectory();

            string path1 = Path.GetFullPath(Path.Combine(AppDomain.CurrentDomain.SetupInformation.ApplicationBase, @"..\..\..\..\..\"));
            string path2 = @$"{path1}src\WebUI\HR.Job.WebApp\wwwroot\test\dataset.csv";

            using (var stream = File.OpenRead(path2))
            {
                dto = new EmployeeAddFileReqDto
                {
                    File = new FormFile(stream, 0, stream.Length, null!, Path.GetFileName(stream.Name))
                };

                List<EmployeeAddFileResDto> dtoRes = new List<EmployeeAddFileResDto>();

                var result = emp.AddFile(dto);
                csvCount = result.Count();
            }
        }

        csvCount.Should().BeGreaterThan(param1);
    }

    [Test, Order(2)]
    public void GetListTest()
    {
        int count = 0;
        using (var scope = Testing._scopeFactory.CreateScope())
        {
            var emp = scope.ServiceProvider.GetRequiredService<IEmployeeService>();

            var list = emp.GetList(new HR.Job.Application.Services.EmployeeGetListReqDto { });
            count = list.Rows.Count();
        }

        count.Should().Be(2);
    }

    [Test, Order(3)]
    public void CreateTest()
    {

        var dto = new EmployeeCreateReqDto
        {
            Mobile = "909163008",
            PayrollNumber = "1",
            Postcode = "2",
            Address = "Address",
            Address2 = "Address2",
            StartDate = DateTime.Now,
            DateOfBirth = DateTime.Now,
            EmailHome = "dddd@gmail.com",
            Forename = "Forename",
            Surname = "Surname",
            Telephone = "909163008"
        };

        using (var scope = Testing._scopeFactory.CreateScope())
        {
            var emp = scope.ServiceProvider.GetRequiredService<IEmployeeService>();

            testId = emp.Create(dto);
        }

        dto.Id.Should().BeGreaterThan(0);
    }

    [Test, Order(4)]
    public void GetByIdTest()
    {

        EmployeeGetByIdResDto dto = null;
        int count = 0;
        using (var scope = Testing._scopeFactory.CreateScope())
        {
            var emp = scope.ServiceProvider.GetRequiredService<IEmployeeService>();

            dto = emp.GetById(testId);
        }

        dto.Id.Should().Be(testId);
    }
    [Test, Order(5)]
    public void EditTest()
    {
        EmployeeGetByIdResDto resDto = null;

        var dto = new EmployeeUpdateReqDto
        {
            Id = testId,
            Mobile = "909163008",
            PayrollNumber = "1",
            Postcode = updatePostVal,
            Address = "Address",
            Address2 = "Address2",
            StartDate = DateTime.Now,
            DateOfBirth = DateTime.Now,
            EmailHome = "dddd@gmail.com",
            Forename = "Forename",
            Surname = "Surname",
            Telephone = "909163008"
        };

        using (var scope = Testing._scopeFactory.CreateScope())
        {
            var emp = scope.ServiceProvider.GetRequiredService<IEmployeeService>();

            emp.Update(dto);

            resDto = emp.GetById(testId);
        }

        resDto.Postcode.Should().Be(updatePostVal);
    }

    [Test, Order(6)]
    public void DeleteTest()
    {
        EmployeeGetByIdResDto resDto = null;


        using (var scope = Testing._scopeFactory.CreateScope())
        {
            var emp = scope.ServiceProvider.GetRequiredService<IEmployeeService>();

            emp.Delete(testId);

            resDto = emp.GetById(testId);
        }

        resDto.Should().Be(null);
    }

    [Test, Order(7)]
    public void Cleanup()
    {
        bool passed = TestContext.CurrentContext.Result.Outcome.Status == NUnit.Framework.Interfaces.TestStatus.Passed;
        using (var scope = Testing._scopeFactory.CreateScope())
        {
            var context = scope.ServiceProvider.GetRequiredService<AppDbContext>();

            int deletedCount = context.Employees.ExecuteDelete();

            deletedCount.Should().Be(csvCount);
        }
    }
}
