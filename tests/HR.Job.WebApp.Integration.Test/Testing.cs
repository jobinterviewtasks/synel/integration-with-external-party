using HR.Job.Repository;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;

namespace HR.Job.WebApp.Integration.Test;

[SetUpFixture]
public partial class Testing
{   
    private static WebApplicationFactory<Program> _factory = null!;
    public static IServiceScopeFactory _scopeFactory = null!;

    [OneTimeSetUp]
    public async Task RunBeforeAnyTests()
    {
        _factory = new CustomWebApplicationFactory();
        _scopeFactory = _factory.Services.GetRequiredService<IServiceScopeFactory>();

        try
        {
            using(var scope = _scopeFactory.CreateScope())
            {
                var _context = scope.ServiceProvider.GetRequiredService<AppDbContext>();
                if (_context.Database.IsNpgsql())
                {
                    await _context.Database.MigrateAsync();
                }
            }
        }
        catch (Exception ex)
        {
            throw;
        }
    }

    [OneTimeTearDown]
    public void RunAfterAnyTests()
    {
    }
}
